///////// Initialize Environment ////////////

var canvas = document.getElementById("canvas");
var tempCanvas = document.getElementById("canvas_temp");

canvas.width = 1000;
canvas.height = 780;
tempCanvas.width = canvas.width;
tempCanvas.height = canvas.height;

//The Real Canvas to be displayed.
var context =     canvas.getContext("2d");
var tempContext = tempCanvas.getContext("2d");
var ctx  = context; //ctx might be different for temporary drawing [when draw(false)].

canvas.addEventListener("mousedown", mouseDown, false);
canvas.addEventListener("mousemove", mouseMove, false);
canvas.addEventListener("mouseup", mouseUp, false);

// // For out canvas range.
// window.addEventListener("Win_mousedown", WmouseDown, false);
// window.addEventListener("Win_mousemove", WmouseMove, false);
// window.addEventListener("Win_mouseup", WmouseUp, false);


tempCanvas.addEventListener("mousedown", mouseDown, false);
tempCanvas.addEventListener("mousemove", mouseMove, false);
tempCanvas.addEventListener("mouseup", mouseUp, false);

// ctx stroke style.
ctx.lineJoin = "round"; //連接處
ctx.lineCap = "round";  //末端
// initial color
ctx.strokeStyle ="red";
ctx.fillStyle = "red";
// initial Size.
ctx.lineWidth = 25

/////////////////// Variables ////////////////
// true:  Now this object.
// false: Not this object.
var isPen = true;
var isEraser = false;
var isRect = false;
var isCircle = false;
var isTri = false;
var isRectFill = false;
var isCircleFill = false;
var isTriFill = false;
var isText = false;
// True :Down, False:Up.
var mouseIsDown = false;

// Coordinates. 
var positionX, positionY; // For mouse position.
var startX, endX, startY, endY; // Critical Points for Polygons.
var TextStartpos = 0; //Starting point of Text.

//////////////// Get HTML elements. ///////////////////// When user click on the icons, should react.

var brush = document.getElementById("brush");
brush.addEventListener("click", brushClick); //Brush click event 

var eraser = document.getElementById("eraser");
eraser.addEventListener("click", eraserClick); //Eraser click event

///////// Pencil / Eraser /Text ///////////
function brushClick() {
    isPen = true;

    isEraser = false;
    isCircle = false;
    isRect = false;
    isTri = false;
    isRectFill = false;
    isCircleFill = false;
    isTriFill = false;
    isText = false;
    
    ctx.strokeStyle = rgbaColor;
    console.log(rgbaColor);
    document.body.style.cursor = 'url("brush_cursor.png"),auto';

    canvas.addEventListener("mousedown", mouseDown, false);
    canvas.addEventListener("mousemove", mouseMove, false);
    canvas.addEventListener("mouseup", mouseUp, false);
}
function eraserClick() {
    
    isEraser = true;

    isPen = false;
    isCircle = false;
    isRect = false;
    isTri = false;
    isRectFill = false;
    isCircleFill = false;
    isTriFill = false;
    isText = false;

    ctx.strokeStyle = "white";
    document.body.style.cursor = 'url("erase_cursor.png"),auto';

    console.log("eraser here");

    canvas.addEventListener("mousedown", mouseDown, false);
    canvas.addEventListener("mousemove", mouseMove, false);
    canvas.addEventListener("mouseup", mouseUp, false);
}



////// Reset / Undo / Redo /////////

var reset = document.getElementById("reset");
var undo = document.getElementById("undo");
var redo = document.getElementById("redo");
reset.addEventListener("click", resetClick); //Reset click event 
undo.addEventListener("click", cUndo);
redo.addEventListener("click", cRedo);

var cPushArray = new Array();
var cStep = -1;	

function resetClick() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    cPush.length = 0;   // clear cPushArray.
    cPush();            // Record this 'reset' action.
    cStep = -1;         // -1 means. we don't undo.
}
function cPush() {
    console.log("Push!");
    console.log(cStep);
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('canvas').toDataURL()); // Push every timestep of Canvas into this Big Stack. (First-in Last-out.)
}
function cUndo() {
    if (cStep > 0) {    // we can undo.
        cStep--;
        var canvasPic = new Image();        //every time new an Image(); 
        canvasPic.src = cPushArray[cStep];  //Get last moment canvas information.
        ctx.clearRect(0, 0, canvas.width, canvas.height); // Clear the whold canvas.
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); } // Place our last result on it. 
        // .onload : use for waiting.
    }
    else if (cStep == 0) {  //Let cstep be -1.
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        cStep--;
    }
    else{
        console.log(" cStep < 0!");
    }
}
function cRedo() {
    if (cStep < cPushArray.length-1) {  // the Latest moment, we cannot Redo! Since we don't know the future.
        cStep++;       // move to next moment.
        var canvasPic = new Image();        // new an Image 
        canvasPic.src = cPushArray[cStep];  ///Get next moment canvas information.
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); } // Place our next result on it. 
    }
}


//////// Upload / Download /////////

var uploadLink = document.getElementById("uploadLink");
uploadLink.addEventListener('change', uploadClick, false);

var downLoadLink = document.getElementById("saveLink");
downLoadLink.addEventListener("click", saveClick); //Download click event 

var canUpload = false;

function uploadClick(e){
    canUpload = true;
    isPen = false;
    console.log("Upload an image.");
    // Upload = true;
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();    // new an image object.
        console.log("new an image.");

        img.onload = function(){
            // Click on canvas.
            tempCanvas.onclick = function (evt) {
                var mousePos = getMousePos(canvas, evt);
                ctx.drawImage(img,mousePos.x,mousePos.y,100, 100);
                cPush();
                this.onclick = null;
            };
            // tempCanvas.onclick = function(e) {
            //     var rect = canvas.getBoundingClientRect();
            //     if (Upload===true){                                                     // width, height.
            //         ctx.drawImage(img, e.clientX - rect.left , e.clientY - rect.top, 100, 100);
            //         Upload = false;
            //     }else{
            //         console.log("Should press button again.");
            //     }
            // }           
        }
        img.src = event.target.result;
        console.log("give img source."); 
    }
    reader.readAsDataURL(e.target.files[0]);    
    console.log("Read data URL.");
    onclick="this.value=null"
}

function saveClick() {
	var data = canvas.toDataURL(); //encodes your canvas-image information into a base 64 format
	downLoadLink.href = data;
    downLoadLink.download = "Canvas_Masterpiece.png";
    console.log(data);
}



// Six operations. //
var circle = document.getElementById("circle");
var rectangle = document.getElementById("rectangle");
var triangle = document.getElementById("triangle");
var circle_fill = document.getElementById("circle_fill");
var rectangle_fill = document.getElementById("rectangle_fill");
var triangle_fill = document.getElementById("triangle_fill");

circle.addEventListener("click", drawCircle);
rectangle.addEventListener("click", drawRect);
triangle.addEventListener("click", drawTri);
circle_fill.addEventListener("click", drawCircleFill);
rectangle_fill.addEventListener("click", drawRectFill);
triangle_fill.addEventListener("click", drawTriFill);



////// Draw Objects //////
////// Just set its 'flag' = true, others' false. //////
////// And set color and size. //////

function drawCircle() {
    isPen = false;
    isEraser = false;
    isCircle = true;
    isRect = false;
    isTri = false;
    isRectFill = false;
    isCircleFill = false;
    isTriFill = false;
    isText = false;

    tempContext.strokeStyle =  rgbaColor;
    ctx.lineWidth = slider.value;
}
function drawCircleFill() {
    isPen = false;
    isEraser = false;
    isCircle = false;
    isRect = false;
    isTri = false;
    isRectFill = false;
    isCircleFill = true;
    isTriFill = false;
    isText = false;

    ctx.lineWidth = slider.value;
    tempContext.fillStyle =rgbaColor;

}
function drawRect() {
    isPen = false;
    isEraser = false;
    isCircle = false;
    isRect = true;
    isTri = false;
    isRectFill = false;
    isCircleFill = false;
    isTriFill = false;
    isText = false;

    ctx.lineWidth = slider.value;
    tempContext.strokeStyle =  rgbaColor;
}
function drawRectFill() {
    isPen = false;
    isEraser = false;
    isCircle = false;
    isRect = false;
    isTri = false;
    isRectFill = true;
    isCircleFill = false;
    isTriFill = false;
    isText = false;

    tempContext.fillStyle =rgbaColor;
    ctx.lineWidth = slider.value;
}
function drawTri() {
    isPen = false;
    isEraser = false;
    isCircle = false;
    isRect = false;
    isTri = true;
    isRectFill = false;
    isCircleFill = false;
    isTriFill = false;
    isText = false;

    tempContext.strokeStyle =  rgbaColor;
    ctx.lineWidth = slider.value;
}
function drawTriFill() {
    isPen = false;
    isEraser = false;
    isCircle = false;
    isRect = false;
    isTri = false;
    isRectFill = false;
    isCircleFill = false;
    isTriFill = true;
    isText = false;
 
    tempContext.fillStyle =rgbaColor;
    ctx.lineWidth = slider.value;
}



//////////// Music Select //////////////////

var music = document.getElementById("music");    //music.source.

function ChangeMusic(src){
    music.src = src;
    console.log("Change Music.");
}


///////////// Change cursor ///////////////////////
function changeCursor(url){
    if(url == 'text'){
        canvas.style.cursor = url;
        tempCanvas.style.cursor = url;
    }
    else{
        canvas.style.cursor = "url("+url+"),auto";
        tempCanvas.style.cursor = "url("+url+"),auto";
    }
};




//////////// Brush Size /////////////
var slider = document.getElementById("myRange");
var BrushSize = document.getElementById("Brush_Size");
BrushSize.innerHTML = 25;

// Update the current slider value (each time you drag the slider handle)
slider.oninput = function() {
    ctx.lineWidth = this.value;
    BrushSize.innerHTML = this.value;
    console.log(BrushSize.innerHTML);
}

// document.addEventListener("keydown", function(e) {
//     if(e.keyCode == 37) { // left arrow.
//         if(slider.value >1){
//             ctx.lineWidth -= 1;
//             BrushSize.innerHTML.value -= 1 ;
//             slider.value -=1;
//         }
//     } 
//     else if(e.keyCode == 39) { // Right arrow.
//         if(slider.value <50){
//             ctx.lineWidth += 1;
//             BrushSize.innerHTML.value = BrushSize.innerHTML.value+ 1;
//             slider.value = slider.value +1;
//         }
//     } 
//     else {
//         // do nothing.
//         console.log("press " + e.keyCode);
//     }     
// }, false);

// document.addEventListener("keyup", function(e) {
//      console.log("Change brush Size using keydown-up." + "BrushSize "+BrushSize.innerHTML.value);
//      console.log(slider.value);
//      cPush();
// })





/////////// Color Selector ////////////////
var colorLabel = document.getElementById('color-label');

var colorBlock = document.getElementById('color-block');
var ctx1 = colorBlock.getContext('2d');
var width1 = colorBlock.width;
var height1 = colorBlock.height;

var colorStrip = document.getElementById('color-strip');
var ctx2 = colorStrip.getContext('2d');
var width2 = colorStrip.width;
var height2 = colorStrip.height;

var x = 0;
var y = 0;
var drag = false;
var rgbaColor = 'rgba(255,0,0,1)';

ctx1.rect(0, 0, width1, height1);
fillGradient();

ctx2.rect(0, 0, width2, height2);
var grd1 = ctx2.createLinearGradient(0, 0, 0, height1);
grd1.addColorStop(0, 'rgba(255, 0, 0, 1)');
grd1.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
grd1.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
grd1.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
grd1.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
grd1.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
grd1.addColorStop(1, 'rgba(255, 0, 0, 1)');
ctx2.fillStyle = grd1;
ctx2.fill();

colorStrip.addEventListener("click", click, false);

colorBlock.addEventListener("mousedown", Color_mousedown, false);
colorBlock.addEventListener("mousemove", Color_mousemove, false);
colorBlock.addEventListener("mouseup", Color_mouseup, false);
function click(e) {
  x = e.offsetX;
  y = e.offsetY;
  var imageData = ctx2.getImageData(x, y, 1, 1).data;
  rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
  colorLabel.style.backgroundColor = rgbaColor;
  ctx.strokeStyle = rgbaColor;
  fillGradient();

}
function fillGradient() {
  ctx1.fillStyle = rgbaColor;
  ctx1.fillRect(0, 0, width1, height1);

  var grdWhite = ctx2.createLinearGradient(0, 0, width1, 0);
  grdWhite.addColorStop(0, 'rgba(255,255,255,1)');
  grdWhite.addColorStop(1, 'rgba(255,255,255,0)');
  ctx1.fillStyle = grdWhite;
  ctx1.fillRect(0, 0, width1, height1);

  var grdBlack = ctx2.createLinearGradient(0, 0, 0, height1);
  grdBlack.addColorStop(0, 'rgba(0,0,0,0)');
  grdBlack.addColorStop(1, 'rgba(0,0,0,1)');
  ctx1.fillStyle = grdBlack;
  ctx1.fillRect(0, 0, width1, height1);
}
function Color_mousedown(e) {
  drag = true;
  changeColor(e);
}
function Color_mousemove(e) {
  if (drag) {
    changeColor(e);
  }
}
function Color_mouseup(e) {
  drag = false;
}
function changeColor(e) {
  x = e.offsetX;
  y = e.offsetY;
  var imageData = ctx1.getImageData(x, y, 1, 1).data;
  rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
  colorLabel.style.backgroundColor = rgbaColor;
  ctx.strokeStyle = rgbaColor;
  // For text.
  ctx.fillStyle = rgbaColor;
}


/////////// Font Selector (Style / Size) /////////////

function Type_Text(){ // Click Text icon.
    isPen = false;
    isEraser = false;
    isCircle = false;
    isRect = false;
    isTri = false;
    isRectFill = false;
    isCircleFill = false;
    isTriFill = false;
    isText = true;
    document.body.style.cursor = 'text';
}

// Default setting.
var hasInput = false;
var font_family = "Arial";
var font_size = "12px";

function fontFam(val){
    font_family = val;
    console.log("Change fontFam");
}
function fontSize(val){
    font_size = val;
    console.log("Change fontSize");
}

// document.addEventListener("keydown", function(e) {
//     if(e.keyCode == 38) { // Up arrow.
//         font_size = font_size +1;
//     } 
//     else if(e.keyCode == 40) { // Down arrow.
//         font_size = font_size -1;
//     } 
//     else {
//         // do nothing.
//         console.log("press " + e.keyCode);
//     }     
// }, false);

// document.addEventListener("keyup", function(e) {
//     //  console.log("Change brush Size using keydown-up." + "BrushSize "+BrushSize.innerHTML.value);
//     //  console.log(slider.value);
//      cPush();
// })



// Click on canvas.
tempCanvas.onclick = function(e) {
    var rect = canvas.getBoundingClientRect();
    if (hasInput || isText !== true) return;
    addInput(e.clientX /*- rect.left*/ , e.clientY /*- rect.top*/); 
};

function addInput(x, y) {

    var input = document.createElement('input');

    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (x) + 'px';
    input.style.top = (y) + 'px';
    
    input.onkeydown = handleEnter;
    document.body.appendChild(input);

    // input.focus();
    hasInput = true;
};

function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {   //New line.
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        hasInput = false;
    }
};

function drawText(txt, x, y) {
    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.font = font_size + "px " + font_family;
    console.log(font_size);
    console.log(font_family);
    console.log(x);
    console.log(y);
    ctx.fillText(txt, x-150 , y-35 );
    cPush();
};


// Get Bounding Client Rect.
function getCoordinates(canvas, e) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top
    };
}

function mouseDown(eve) {
    if(isPen) {
            mouseIsDown = true;
            var coordinates = getCoordinates(canvas, eve);
            positionX = coordinates.x;
            positionY = coordinates.y;
            ctx.beginPath();
                ctx.moveTo(positionX, positionY);
                ctx.lineTo(positionX, positionY);
                ctx.stroke();
    }
    else if(isEraser) {
            mouseIsDown = true;
            var coordinates = getCoordinates(canvas, eve);
            positionX = coordinates.x;
            positionY = coordinates.y;
            ctx.beginPath();
            ctx.moveTo(positionX, positionY);
            ctx.lineTo(positionX, positionY);
            ctx.stroke();
    }
    else {
            mouseIsDown = true;
            var pos = getMousePos(canvas, eve);
            startX = endX = pos.x;
            startY = endY = pos.y;
            draw(false); //update
            console.log("shape");
    }
}

function mouseUp(eve) {
    if(isPen || isEraser) {
        mouseIsDown = false;
        cPush();
    }
    else {
        if (mouseIsDown !== false) { //If mouse from Down to UP.
            mouseIsDown = false;
            var pos = getMousePos(canvas, eve);
            endX = pos.x;
            endY = pos.y;
            draw(true); 
            cPush();
        }
    }
}
// var out_of_range = false;
function mouseMove(eve) {
    console.log(mouseIsDown);
    if(mouseIsDown==true){
        if(isPen || isEraser) {
            var coordinates = getCoordinates(canvas, eve);
            positionX = coordinates.x;
            positionY = coordinates.y;
            // if(positionX > canvas.width || positionX < 0 || positionY >= canvas.height || positionY <=0){
            //     out_of_range = true; //if true: draw.
            //     draw(true);
            // }
            if(isPen) document.body.style.cursor = 'url("brush_cursor.png"),auto';
            else  document.body.style.cursor = 'url("erase_cursor.png"),auto';
            // if(positionX <= canvas.width && positionX >= 100 && positionY <= canvas.height && positionY >=20){
                brushDraw(canvas, positionX, positionY);
        }
        else if (mouseIsDown !== false) { //If mouse is down do ; else: Mouse is UP , do nothing!
            var pos = getMousePos(canvas, eve);
            endX = pos.x;
            endY = pos.y;
            // if(endX > canvas.width || endX < 0 || endY >= canvas.height || endY <=0){
            //     out_of_range = true; //if true: draw.
            //     draw(true);
            // }
            // else{
                draw(false);
            // }
        }
    }
}

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect(); // Get the border of the Canvas.
    //evt.clientX is the absolute value of mouse x w.r.t. the window.
    //So we should minus the starting point of the canvas in order to get the 'relative coord.' w.r.t. canvas.
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

// Draw Pencil or Other Objects. (during mouseMove).
function brushDraw(canvas, positionX, positionY) {
    if(mouseIsDown) {
        ctx.lineTo(positionX, positionY);
        ctx.stroke();
        if(isEraser) {
            document.body.style.cursor = 'url("erase_cursor.png"),auto';
        }
        else {
            document.body.style.cursor = 'url("brush_cursor.png"),auto';
        } 
    }
}
function draw(final) {

    if (final == true) {   // final draw
        ctx = context;        // clear temp canvas
        tempContext.clearRect(0, 0, canvas.width, canvas.height);
    }
    else { // temporary draw
        ctx = tempContext; // clear true canvas
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
    //Update color.
    ctx.strokeStyle = rgbaColor;
    ctx.fillStyle = rgbaColor;
    // Calculating parameters.
    var w = endX - startX;
    var h = endY - startY;
    var offsetX = (w < 0) ? w : 0;
    var offsetY = (h < 0) ? h : 0;
    var width = Math.abs(w);
    var height = Math.abs(h);
    var radius = Math.sqrt(Math.pow((startX - endX), 2) + Math.pow((startY - endY), 2))

    ctx.lineWidth = slider.value;
    if(isCircle) {
        ctx.beginPath();
        ctx.arc(startX + offsetX, startY + offsetY, radius, 0, 2 * Math.PI);
        ctx.closePath();
        ctx.stroke();
    }
    if(isRect) {
        ctx.beginPath();
        ctx.rect(startX, startY , w, h);
        ctx.closePath();
        ctx.stroke();
    }
    if(isTri) {
        ctx.beginPath();
        ctx.moveTo(startX, startY);
        ctx.lineTo(startX + w, startY + h);
        ctx.lineTo(startX - w, startY + h);
        ctx.closePath();
        ctx.stroke();
    }
    if(isCircleFill) {
        ctx.beginPath();
        ctx.arc(startX + offsetX, startY + offsetY, radius, 0, 2 * Math.PI);
        ctx.closePath();
        ctx.fill();
    }
    if(isRectFill) {
        ctx.beginPath();
        ctx.rect(startX, startY , w, h);
        ctx.closePath();
        ctx.fill();
    }
    if(isTriFill) {
        ctx.beginPath();
        ctx.moveTo(startX, startY);
        ctx.lineTo(startX + w, startY + h);
        ctx.lineTo(startX - w, startY + h);
        ctx.closePath();
        ctx.fill();
    }
    console.log("here");
}










// TODO 1 : ADD Keyboard l/right to change brush size.

// TODO 2 :Should Fixed the out of range. //
// TODO 3 : Why cannot put the same image twice or more? //